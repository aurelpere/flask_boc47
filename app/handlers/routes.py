from flask import request, render_template, flash
from send_mail import send  # pylint: disable=import-error,no-name-in-module


def configure_routes(app):

    @app.route('/', methods=['GET', 'POST'])
    def hello():
        if request.method == 'POST':
            from_ = request.form['email'].strip()
            name = request.form['name'].strip()
            subject = request.form['subject'].strip()
            content = request.form['message'].strip()
            print(from_)
            print(name)
            print(subject)
            print(content)
            if not from_ or not name or not subject or not content:
                flash(
                    'Il faut remplir tous les champs pour soumettre un message'
                )
                #return render_template('thanks.html')
            else:
                send(from_, name, subject, content)
                return render_template('merci.html')
        else:
            return render_template('index.html')

    @app.route('/merci', methods=['GET'])
    def merci():
        return render_template('merci.html')
