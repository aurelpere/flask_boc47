#!/usr/bin/python3
# coding: utf-8
"""
this is app.py for flask-tutorial (sondage example)
"""
from flask import Flask, request
# pylint: disable=import-error
from handlers.routes import configure_routes
from flask import logging

# create the Flask app

app = Flask(__name__)
app.config['SECRET_KEY'] = '36b788e06d0c84d524eb881879ac7e2f66352b6480717bb9'
log = logging.create_logger(app)
configure_routes(app)

#from logging.handlers import SMTPHandler

#mail_handler = SMTPHandler(
#    mailhost='127.0.0.1',
#    fromaddr='server-error@example.com',
#    toaddrs=['admin@example.com'],
#    subject='Application Error'
#)
#mail_handler.setLevel(logging.ERROR)
#mail_handler.setFormatter(logging.Formatter(
#    '[%(asctime)s] %(levelname)s in %(module)s: %(message)s'
#))

#if not app.debug:
#    app.logger.addHandler(mail_handler)


@app.before_request
def log_request_info():
    log.debug('Headers: %s', request.headers)
    log.debug('Body: %s', request.get_data())


#logging.basicConfig(filename='error.log',level=logging.DEBUG)
if __name__ == '__main__':
    # run app in debug mode on port 5000
    app.run(debug=True, host="0.0.0.0", port=5000)
