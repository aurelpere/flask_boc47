from flask import Flask
from app.handlers.routes import configure_routes  # pylint: disable=import-error,no-name-in-module

#http://localhost:5000/?answeryes=1&answerno=0&hash=8651738539259819158


def test_root():
    app = Flask(__name__)
    configure_routes(app)
    client = app.test_client()
    url = '/'
    response = client.get(url)
    assert response.status_code == 200


def test_merci():
    app = Flask(__name__)
    configure_routes(app)
    client = app.test_client()
    url = '/merci'
    response = client.get(url)
    assert response.status_code == 200
